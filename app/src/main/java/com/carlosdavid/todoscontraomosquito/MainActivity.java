package com.carlosdavid.todoscontraomosquito;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        AlteraActionBar();


    }

    private void AlteraActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setTitle("Todos Contra o Mosquito!");
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.backgroundcolor));
    }

    @Override
public boolean onCreateOptionsMenu(Menu menu)
{
getMenuInflater().inflate(R.menu.menu, menu);
    return(true);
}
    @Override
    public boolean onMenuItemSelected(int panel, MenuItem item){


        return (true);
    }

    public void ClickLayou1(View view){
       // Toast.makeText(this,"layout1",Toast.LENGTH_SHORT).show();
    }
    public void ClickLayou2(View view){
        //Toast.makeText(this,"layout2",Toast.LENGTH_SHORT).show();
    }
}
